#!/bin/bash

TOOLS_PATH=$( echo $(realpath "$BASH_SOURCE") | sed "s/$(basename $(realpath "$BASH_SOURCE" ))//")
OPTIONS_PATH="${TOOLS_PATH}options.yaml"
TEMPLATE_PATH="${TOOLS_PATH}template.yaml"
LAYOUTS_PATH="${TOOLS_PATH}layouts.yaml"

USE_NVIDIA=$( (nvidia-smi >/dev/null 2>&1 && ffmpeg -hide_banner -filters | grep overlay_cuda >/dev/null) && echo -n "ok" || echo -n "error")
OVERLAY_FILTER=$( [[ $USE_NVIDIA == 'ok' ]] && echo "overlay_cuda" || echo "overlay" )
HWUPLOAD=$( [[ $USE_NVIDIA == 'ok' ]] && echo ",hwupload_cuda" || echo "" )
HWACCEL=$( [[ $USE_NVIDIA == 'ok' ]] && echo "-hwaccel cuda" || echo "" )
INPUT_CUVID=$( [[ $USE_NVIDIA == 'ok' ]] && echo "-c:v h264_cuvid" || echo "" )
CODEC_OUTPUT=$( [[ $USE_NVIDIA == 'ok' ]] && echo "-c:v h264_nvenc -preset p4" || echo "-c:v libx264 -preset fast" )
USE_GL_TRANSITIONS=$( ffmpeg -hide_banner -loglevel error -filters | grep gltransition >/dev/null && echo -n 1 || echo -n 0)

SPEECH_SPEED=180

get_random_option (){
  option_path="$1"
  fil="${2:-$OPTIONS_PATH}"
  
  if [[ "${fil}" == "${OPTIONS_PATH}" ]] ; then
    option_path=".options.${option_path}"
  fi
  
  length=$(yq eval "(${option_path} | length) - 1" "${fil}")
  if [[ "${length}" -ge 0 ]] ; then
    selected=$(shuf -i 0-${length} -n 1)
    yq eval ${option_path}[${selected}] ${fil} 2>/dev/null
  else
    echo ""
  fi
}

get_random_layout (){
  length=$(yq eval "(.layouts | length) - 1" "${LAYOUTS_PATH}")
  if [[ "${length}" -ge 0 ]] ; then
    selected=$(shuf -i 0-${length} -n 1)
    yq eval ".layouts[${selected}].name" "${LAYOUTS_PATH}" 2>/dev/null
  else
    echo ""
  fi
}

get_random_gl_transition () {
  lista="$(find ${TOOLS_PATH}/gl-transitions -name '*.glsl' 2>/dev/null )"
  cantidad=$(echo -e "${lista}" | wc -l)
  seleccionado=$(shuf -i 1-${cantidad} -n 1)
  echo $(echo -e "${lista}" | head -n $seleccionado | tail -n 1)
}

get_random_output_transition () {
  caller=$(basename $0)
  xfadetrans=$(get_random_option xfade_transitions)
  yq eval ".options.transitions.out.xfade" ${OPTIONS_PATH} \
    | sed "s/DURATION/$((${DURATION}-1))/g" \
    | sed "s/WIDTH/${WIDTH}/g" \
    | sed "s/HEIGHT/${HEIGHT}/g" \
    | sed "s/XFADETRANS/${xfadetrans}/g" \
    | sed "s/OUTPUTNUMBER/${1}/g" 
}

detect_input () {
  FILE_INPUT="$1"
  localw="CWIDTH$2"
  localh="CHEIGHT$2"
  if [[ -z "${FILE_INPUT}" ]] ; then
    # echo "[$(date)] - $0 - No input #${2} detected. Generating ${DURATION} seconds video with random color."
    echo "-f lavfi -i color=$(get_random_option colors):size=${!localw}x${!localh}:r=30"
  else
    extension=$( echo -n "${FILE_INPUT^^}" | tr '.' ' ' | awk '{print $NF}')
    if [[ "$extension" == "JPG" || "$extension" == "JPEG" || "$extension" == "PNG" || "$extension" == "GIF" ]] ; then
      # echo "[$(date)] - $0 - Image input detected for input #${2}. Generating ${DURATION} seconds video."
      echo "-loop 1 -i ${FILE_INPUT}"
    elif [[ $(echo "$extension" | grep "MPEG\|MPG\|MP4\|MKV\|AVI\|M4V\|OGV" 2>/dev/null) != "" ]] ; then
      # echo "[$(date)] - $0 - Video input detected for input #${2}."
      echo "-i ${FILE_INPUT}"
    else
      # echo "[$(date)] - $0 - WARNING: unknown input type detected for input #${2}. Passing it directly to multimedia pipeline."
      echo "-i ${FILE_INPUT}"
    fi
  fi
}

process_filter_input () {
  FILTER_INPUT="$1"
  index="$2"

  if [[ -z "${FILTER_INPUT}" ]] ; then
    # empty filter
    echo ""
  else
    # filter stablished. Added trailing comma"
    echo ",${FILTER_INPUT}"
  fi
}

get_duration_in_seconds() {
  file="$1"
  
  if [[ -z "${file}" || ! -f "${file}" ]] ; then
    echo "[$(date)] - get_duration_in_seconds - ERROR: no se puede encontrar el archivo '${file}'."
  fi
  
  ffprobe -hide_banner -loglevel error \
    -show_entries format=duration \
    -of default=noprint_wrappers=1:nokey=1  "${file}" 2>&1 \
    | awk 'function ceil(valor){ return (valor == int(valor)) ? valor : int(valor)+1} { printf "%d", ceil($1)}'
}
