#!/bin/bash 

source "$(dirname $0)/../common.inc"

usage() {
  echo "Usage: "
  echo
  echo "$0 [--width|-w number] [--height|-h number] -f filename"
  echo
  echo "  --input1 | -i1 filename"
  echo "    Optional"
  echo "    Name of the input file to be put inside the content area #1"
  echo "    Default: auto-generated 5 seconds content."
  echo
  echo "  --input2 | -i2 filename"
  echo "    Optional"
  echo "    Name of the input file to be put inside the content area #2"
  echo "    Default: auto-generated 5 seconds content."
  echo
  echo "  --output | -o filename"
  echo "    Optional"
  echo "    Name of the output file to be generated. Default: output.mkv"
  echo
  echo "  --duration | -d number"
  echo "    Optional"
  echo "    Duration for the output video. Default: input duration."
  echo
  echo "  --width | -w number"
  echo "    Optional"
  echo "    Width, in pixels, of the whole video canvas. Default: 1920"
  echo
  echo "  --height | -h number"
  echo "    Optional"
  echo "    Height, in pixels, of the whole video canvas. Default: 1080"
  echo
  echo "  --content_width | -cw number"
  echo "    Optional"
  echo "    Width, in pixels, of the content area. Default: 1728"
  echo
  echo "  --content_height | -ch number"
  echo "    Optional"
  echo "    Height, in pixels, of the content area. Default: 972"
  echo
  echo "  --content_x | -cx number"
  echo "    Optional"
  echo "    Position, in pixels, of the top border for the content."
  echo "    Default: 96"
  echo
  echo "  --content_y | -cy number"
  echo "    Optional"
  echo "    Position, in pixels, of the left border for the content."
  echo "    Default: 54"
}


WIDTH=1920
HEIGHT=1080
CWIDTH1=1920
CHEIGHT1=540
POSX1=0
POSY1=0
CWIDTH2=1920
CHEIGHT2=540
POSX2=${POSX1}
POSY2=$(( ${CHEIGHT1} + ${POSY1} ))
DURATION=5
FILEOUTPUT="output.mkv"

while [ $# -gt 0 ]; do
  case "$1" in
    --width*|-w*)
      if [[ "$1" != *=* ]]; then shift; fi
      WIDTH="${1#*=}"
      ;;
    --height*|-h*)
      if [[ "$1" != *=* ]]; then shift; fi
      HEIGHT="${1#*=}"
      ;;
    --input1*|-i1*)
      if [[ "$1" != *=* ]]; then shift; fi
      FILEINPUT1="${1#*=}"
      ;;
    --input2*|-i2*)
      if [[ "$1" != *=* ]]; then shift; fi
      FILEINPUT2="${1#*=}"
      ;;
    --filter1*|-f1*)
      if [[ "$1" != *=* ]]; then shift; fi
      FILTERINPUT1="${1}"
      ;;
    --filter2*|-f2*)
      if [[ "$1" != *=* ]]; then shift; fi
      FILTERINPUT2="${1}"
      ;;
    --duration*|-d*)
      if [[ "$1" != *=* ]]; then shift; fi
      DURATION="${1#*=}"
      ;;
    --output*|-o*)
      if [[ "$1" != *=* ]]; then shift; fi
      FILEOUTPUT="${1#*=}"
      ;;
    --content_width*|-cw*)
      if [[ "$1" != *=* ]]; then shift; fi
      CWIDTH="${1#*=}"
      ;;
    --content_height*|-ch*)
      if [[ "$1" != *=* ]]; then shift; fi
      CHEIGHT="${1#*=}"
      ;;
    --content_x*|-cx*)
      if [[ "$1" != *=* ]]; then shift; fi
      POSX="${1#*=}"
      ;;
    --content_y*|-cy*)
      if [[ "$1" != *=* ]]; then shift; fi
      POSY="${1#*=}"
      ;;
    --help)
      usage
      exit 0
      ;;
    *)
      >&2 echo "[$(date)] - $0 - ERROR: Invalid argument $1"
      usage
      exit 1
      ;;
  esac
  shift
done

FILEINPUT1=$(detect_input "${FILEINPUT1}" 1)
FILEINPUT2=$(detect_input "${FILEINPUT2}" 2)
FILTERINPUT1=$(process_filter_input "${FILTERINPUT1}" 1)
FILTERINPUT2=$(process_filter_input "${FILTERINPUT2}" 2)

re='^[0-9]+$'
if ! [[ $WIDTH =~ $re ]] ; then
  echo "[$(date)] - $0 - ERROR: bad width '${WIDTH}'."
  usage
  exit 1
fi

if ! [[ $HEIGHT =~ $re ]] ; then
  echo "[$(date)] - $0 - ERROR: bad height '${HEIGHT}'."
  usage
  exit 1
fi

filter="color=#00000000:size=${WIDTH}x${HEIGHT}:r=30,format=yuva420p[base1];"
filter+="[1:v:0]format=yuva420p,scale=${WIDTH}:${HEIGHT},"
filter+="crop=out_w=${CWIDTH1}:out_h=${CHEIGHT1}:x=0:y=${CHEIGHT1}/2,"
filter+="scale=${CWIDTH1}:${CHEIGHT1} ${FILTERINPUT1}[out1];"
filter+="[2:v:0]format=yuva420p,scale=${WIDTH}:${HEIGHT},"
filter+="crop=out_w=${CWIDTH2}:out_h=${CHEIGHT2}:x=0:y=${CHEIGHT2}/2"
filter+="${FILTERINPUT2}[out2];"
filter+="[0:v:0][out1]overlay=${POSX1}:${POSY1}:shortest=1[out3];"
filter+="[out3][out2]overlay=${POSX2}:${POSY2}:shortest=1[out4];"
filter+="[base1][out4]xfade=transition=$(get_random_option xfade_transitions):duration=0.5:offset=0[out5];"

filter+="$(get_random_output_transition 5)"

ffmpeg -y -hide_banner -loglevel error \
  -canvas_size "${WIDTH}x${HEIGHT}" \
  -f lavfi -i "color=#00ff0000:r=30:size=${WIDTH}x${HEIGHT},format=yuva420p" \
   $FILEINPUT1 \
   $FILEINPUT2 \
   -pix_fmt yuva420p \
   -filter_complex "${filter}" \
   -map "[out]"  -c:v ffv1 -level 3 -g 1 -slicecrc 1 -slices 16 -t ${DURATION} \
   "${FILEOUTPUT}" || exit 1
