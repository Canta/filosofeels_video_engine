// shadertoy id = Md3cWN
// code from http://glslsandbox.com/e#30736.1

float size = 100.0;
float speed= 5.0;

float randomize(vec2 coords){
    //http://byteblacksmith.com/improvements-to-the-canonical-one-liner-glsl-rand-for-opengl-es-2-0/
    highp float a = 12.9898;
        highp float b = 78.233;
        highp float c = 43758.5453;
        highp float dt= dot(coords.xy ,vec2(a,b));
        highp float sn= mod(dt,3.14);
        return fract(sin(sn) * c);
}

vec3 getColor(vec2 coords){
    coords.x = coords.x-mod(coords.x, size);
    coords.y = coords.y-mod(coords.y, size);
    
    float r = randomize(coords.xy+vec2(sin(iTime*0.5)));
    float g = randomize(coords.xy * 20.0+vec2(sin(iTime*0.5)));
    float b = randomize(coords.xy * 37.0+vec2(sin(iTime*0.5)));
    return vec3(r,g,b);
}

bool inSize(vec2 coords){
    vec2 box = coords.xy-mod(coords.xy, size);
    vec2 center = box+(size/2.0);
    float size = (tan((iTime * speed)+(randomize(box*18.0)*2.0))/2.0)*(size);
    return (abs(coords.x-center.x) < size && abs(coords.y-center.y) < size);
}


void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    vec2 uv = fragCoord/iResolution.xy;
    vec3 col = vec3(texture(iChannel0,uv+vec2(0.1,0.0)));
    if (inSize(fragCoord)) col = vec3(texture(iChannel0,uv))*getColor(fragCoord);
    fragColor = vec4(col,1.0);
}
