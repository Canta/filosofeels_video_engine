// shadertoy id = llsXW7
/*

MIT License

Copyright (c) 2015-present Brett Jephson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/
void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
	vec2 uv = fragCoord.xy / iResolution.xy;
    vec4 tex = texture(iChannel0, uv);
    
    float val = 0.1 - mod(iChannelTime[0] / 10.0, 0.2);
    
    vec4 rTex = texture(iChannel0, uv - val);
    float red = rTex.r;
    
    vec4 gTex = texture(iChannel0, uv + val);
    float green = gTex.g;
    
    vec4 bTex = texture(iChannel0, uv - 0.2);
    float blue = bTex.b;
    
    vec2 uv2 = vec2(uv.x, mod(iTime, 1.175));
    vec4 tex2 = texture(iChannel0, uv2);
    
    float grey = dot(vec3(red, green, blue), vec3(0.3, 0.59, 0.11));
    
    vec4 tex3 = mix(tex, tex2, 0.1);
    
    fragColor = mix(tex3, vec4(grey, grey, grey, 1.0), 0.2);
    vec2 posMod = mod( fragCoord.xy, vec2( 4.0 ) );
    if(posMod.y < 2.0) { 
     fragColor.rgb -= 0.5;
    }
}

