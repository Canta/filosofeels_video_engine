# filosofeels_video_engine

Text and script based video generation engine for filosofeels videos.

## What is this thing?

It's just a bunch of bash scripts using [ffmpeg](https://www.ffmpeg.org/) for video generation.

The idea is simple: you script videos. You don't invest time and effort learning how to handle graphical video tools: you just write a text, and process it with scripts. It's a different experience for the video-making adventurer, more explicitly focused in the text content of the video, with hopes of getting two results from it:
  
  1. A good enough video for sharing ideas, by focusing on those ideas.
  2. To dramatically reduce the effort needed on video editing (both skills and work) by means of automation.

It's in its firsts steps, but it works already. Here's the first video made with this: https://www.youtube.com/watch?v=HODGNPMkjjA

That video uses a custom version of ffmpeg, with [vf_shadertoy](https://gitlab.com/Canta/ffmpeg_shadertoy_filter) added. That's because I love shadertoys. But you don't need them.

## Requirements:

The scripts needs:
  - bash
  - realpath
  - yq
  - shuf
  - espeak-ng
  - ffmpeg
  
Optional:
  - nVidia card

## Disclaimer about compatibility:

I'm a GNU/Linux user, and this works on GNU/Linux.
I'm neither user, friend, enthusiast, evangelist, customer, partner, employee, or anything other than enemy of Microsoft and their products, including Windows. So I could not care less about how to make this work on that system. Consider this repo explicitly hostile towards Microsoft. Same goes for Apple stuff: I don't use that, I see no reason at all to use that, and I see that stuff hurting people's rights all around the world, so IMHO should be avoided like a disease. If you want this working on those systems, just fork it and do with the code as you please.


## How to use:

### 0.

The first thing you need to do is to clone this repo. For that, open a terminal window, and enter this:

`git clone https://gitlab.com/Canta/filosofeels_video_engine/`

That way, you'll get a `~/filosofeels_video_engine` directory with the source code.
If you clone the repo somewhere else in your system, take note of the directory path, as you'll need it for calling the scripts.

Next, there are three main scripts, related to the three steps in the video making process. But keep in mind you'll also need some basic understanding of [YAML](https://docs.ansible.com/ansible/latest/reference_appendices/YAMLSyntax.html) (it's not hard stuff, don't worry).

### 1.

Write a plain text file with the narrative contents of the video. Be sure of using paragraphs: just split chunks of text with double new-line/carriage-return ("enter" key). Like this:

```
Lorem ipsum etcetera. This would be the first paragraph.

Lorem ipsum again. This is the second paragraph.
```

Once you have that file (let's say it's called "video_001.txt", in the "~/videos" directory), go to the file's directory with the console, and run this command:

`~/filosofeels_video_engine/build_index video_001.txt`

That will create the `~/videos/video_001` directory, with an "index.yaml" file inside. That's the file you'll be working with from now on.

### 2.

Following the example, open `~/videos/video_001/index.yaml` for edition, with your favourite text editor. You'll see there a list of scenes, each of them generated from the paragraphs in the text file. Also, the scenes have other properties added, like a filter list, a background, and several others. But most important, you'll see a list of "fragments", which are the things that need to be worked from now on.

Fragments look like this:

```yaml
#(...)
      - file: "frag_4.mkv"
        layout: "vertical double"
        duration: 7
        text: >
          Es injusto decir que todo el animé es así, porque todes podemos encontrar ejemplos de lo contrario.
        inputs: 
          - ../imgs/scar-photos-1.jpg
          - ../imgs/gits-kusanagi.jpg
        filters: 
          - "shadertoy=shadertoy_file=%TOOLS_PATH%/shadertoys/scanlines_with_zoom.glsl"
          - "shadertoy=shadertoy_file=%TOOLS_PATH%/shadertoys/scanlines_with_zoom.glsl"
        overlays: []
        audio_effects: []
#(...)
      - file: "frag_6.mkv"
        layout: "clean"
        duration: 6
        text: >
          Pero estos personajes son, sin lugar a dudas, un estandard de género.
        inputs: []
        filters: []
        overlays: []
        audio_effects: []
#(...)
```

Basically, each scene has fragments, and each fragment has its own parameters. The most important one is the `layout` parameter. You gotta set that to something found inside `~/filosofeels_video_engine/layouts.yaml` (in the "name" field). That sets the visual behaviour for the fragment (for example, "a single image", "two images splitting the screen horizontally", "four tiled images", and so on).

In the future, there will be lots of newer layouts, but right now we have just a bunch. And layouts have all "inputs", which are the input files used to generate that visual behaviour. For example, if you want to show an image, then the image path is an "input", and goes in that "inputs" list. The output file name is the "file" property of the fragment.

In the example you can also see the usage of "filters", which are simply [ffmpeg filters](https://trac.ffmpeg.org/wiki/FilteringGuide) to apply to the stated inputs. That's actually advanced stuff, so you **should** skip that unless you're familiar with ffmpeg syntax. The point of the mentioning is this: each line of "filters" matches each line of "inputs". So, the first filter applies to the first image, the second filter to the second image, and so on.

But lastly, you can split your scene into arbitrary fragments, just by cutting the text and thinking the video layout. The game there is basically playing with the duration of the fragments in relation to the full duration of the scene. Just do some experiments and see what you get.

Once you have an scene configured, go via console to the `~/videos/video_001/` directory, and run this:

`~/filosofeels_video_engine/process_scene 1`

The script will create another sub-directory, called `scene_000`, where you'll find some files. Just double click (or run some command line player) to play the `~/videos/video_001/scene_000/scene_000.mkv`, and see what happened. You'll see the scene will have speech dialog automatically generated, and some other mostly random stuff.

Whatever you don't set up in the index YAML file, will be choosen randomly by the scripts. For example, if you choose a layout of 4 images, but don't set images, the scripts will generate static-colored images filling the gaps in the layout. And the same goes if you don't pick a layour for the fragment: the scripts just choose a random layout from the list of available layouts.

`process_scene` script have some parameters (con can see them running it without parameters). One of them lets you build just the fragments (without the compiled scene video), and other allows you to avoid audio at all. You can also see the fragment files in the directory after running the script.

Fragments handling is basically the working step in this video pipeline, and requires a more in-depth explanation, so we'll just leave it here from now.

### 3.

After you have all your scenes made, you can just run this command to build the final video:

`~/filosofeels_video_engine/process_index`

That should be ran in the same directory as the `index.yaml` file is located, and will let the final video in the same directory. That's it.



## Notes:

  - This is very early stage. It works, but frankly it's being used only by myself, and it was used so far to make a single video. Expect some breaking changes in the near future.
  
  - Once the thing looks stable, expect it to NOT have any breaking change in any foreseeable future. We value stability and sustainability here over any tendency.
  
  - That last point is important for this other reason: we want to be able to save just the `index.yaml` file, and the fragment inputs, in order to recreate the videos another time. This way we don't need to backup the videos, but a bunch of text files and mostly `<1MB` images. That would never be possible if we keep changing the `index.yaml` spec, so stability is key.
  
  - Some UI is being thinked, but nothing on the way so far. The idea is using just text edition and console whenever possible. However, fragment edition experience would be greatly improved by some simple UI with things like file pickers and layout selectors, without the need of some big and complex UI like video edition software usually tends to be. with this in mind, we'll take a look at ncurses and the like, in order to see if there's some way of doing a productive UI.
  
  - The use of console has also the benefit of being ssh friendly. That way, we could run remote commands, or even using the UI remotely if programmed with some ncurses-like thing.
  
  - Audio needs more love. We'll think about it. No big ideas so far, but we know it's greener than video.
